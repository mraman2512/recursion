l = [5, 4, 7, 38, 49, 2, 94, 21]


def binarySearchRecusively(l, start, end, key):
    if start > end:
        return False
    mid = (start + end) // 2

    if l[mid] == key:
        return True
    elif l[mid] < key:
        return binarySearchRecusively(l, mid + 1, end, key)
    elif l[mid] > key:
        return binarySearchRecusively(l, start, mid - 1, key)


if __name__ == "__main__":
    l.sort()
    key = 945
    if binarySearchRecusively(l, 0, len(l) - 1, key):
        print(f" {key} is Present !")
    else:
        print("Sorry can't find key")
