def keyOptions(key):
    if key == 1:
        return [""]
    elif key == 2:
        return "abc"
    elif key == 3:
        return "def"
    elif key == 4:
        return "ghi"
    elif key == 5:
        return "jkl"
    elif key == 6:
        return "mno"
    elif key == 7:
        return "pqrs"
    elif key == 8:
        return "tuv"
    elif key == 9:
        return "wxyz"


def keyPadCombination(n):
    if n == 0:
        return [""]

    lastNo = n % 10
    remainingNo = n // 10

    res = keyPadCombination(remainingNo)

    val = list()
    for every in keyOptions(lastNo):
        for each in res:
            val.append(str(each) + str(every))
    return val


def keyPadCombination2(n):
    if n == 0:
        return [""]

    lastNo = n % 10
    remainingNo = n // 10

    res = keyPadCombination2(remainingNo)

    val = list()
    for every in res:
        for each in keyOptions(lastNo):
            val.append(str(each) + str(every))
    return val


if __name__ == "__main__":
    print(keyPadCombination2(12))
